import sys
from OpenGL import GL
import cv2.cv2 as cv2
from PyQt5 import QtGui
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQuick import QQuickView


from multiprocessing import Process,Queue

QUEUE_MAX_SIZE = 1000
framesQueue = Queue(QUEUE_MAX_SIZE)

def frameGrabber(rootObject, view, app):
    cap = cv2.VideoCapture(0)
    while(True):
        ret, frame = cap.read()
        frame = cv2.resize(frame, (700,500), cv2.INTER_AREA)
        framesQueue.put(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()

if __name__ == "__main__":
    app = QApplication(sys.argv) 
    view = QQuickView()
    view.setSource(QUrl('preview.qml'))
    rootObject = view.rootObject()
    view.show()
    
    Grabber = Process(target=frameGrabber, args=(rootObject,view,app,))
    Grabber.start()

    while True:
        if framesQueue.qsize() > 0 and framesQueue.qsize() != QUEUE_MAX_SIZE: 
            frame = framesQueue.get()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            frame = QtGui.QImage(frame, 700, 500, QtGui.QImage.Format_RGBA8888)
            rootObject.update_canvas(frame)
            app.processEvents()

    app.exec_()


 