import Qt 4.7


Rectangle {
    id : counterRect

    property int numberOfItems: 2
    signal digitExceeded()

    width : 30
    height : 50

    Rectangle {
        id: timePicker
        color: "#fff"
        clip: true
        visible : true
        anchors.fill: parent

        Rectangle {
            width: parent.width
            height: 1
            y: (parent.height - listView.delegateHeight) / 2
            color: "#D0D0D0"
        }

        Rectangle {
            width: parent.width
            height: 1
            y: (parent.height - listView.delegateHeight) / 2 + listView.delegateHeight
            color: "#D0D0D0"
        }

        ListView {
            id: listView
            x: 0
            width: parent.width
            y: -parent.height * 0.5
            height: parent.height * 2
            property int delegateHeight: height / numberOfItems
            model: 9999
            spacing: 1
            highlightRangeMode: ListView.StrictlyEnforceRange
            preferredHighlightBegin: (height - delegateHeight) / 2
            preferredHighlightEnd: (height + delegateHeight) / 2

            delegate: Item {
                id: contentItem
                width: listView.width
                height: listView.delegateHeight

                Rectangle {
                    anchors.fill: parent
                    color: "#3333ee33"
                    visible: false
                }

                Text {
                    id: innerText
                    text: index % 10
                    anchors.centerIn: parent
                    font.pixelSize: listView.delegateHeight * 0.8
                    font.family: "Times New Roman"
                    color: contentItem.ListView.isCurrentItem ? "black" : "#999"
                }
            }
        }
    }

    function gotoIndex(idx) {
        anim.running = false
        var pos = listView.contentY;
        var destPos;
        listView.positionViewAtIndex(idx, ListView.Center);
        destPos = listView.contentY;
        anim.from = pos;
        anim.to = destPos;
        anim.running = true;
    }

    NumberAnimation { id: anim; target: listView; property: "contentY"; duration: 25 }

    function incrementCount()
    {
        //listView.positionViewAtIndex(listView.currentIndex + 1, ListView.Center);
        gotoIndex(listView.currentIndex + 1)

        if( ( ( ( listView.currentIndex % 10 ) % 9 ) == 0 ) && ( listView.currentIndex % 10 != 0 ) )
        {
            digitExceeded()
        }
    }

    function resetCounter()
    {
        gotoIndex(0)
    }
}
