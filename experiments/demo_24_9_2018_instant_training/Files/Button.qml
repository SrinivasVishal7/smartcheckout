import Qt 4.7

Rectangle {

    width: 240; height: 320;

    property string buttonText : "Button"

    radius: 5

    Text {
        id : buttonTextLabel
        anchors.centerIn: parent
        text : buttonText
        font.family: "Times New Roman"
        font.bold: true
        font.pixelSize: 40
        visible : true
    }

    gradient: Gradient {
                GradientStop { position: 0.0; color: "pink" }
                GradientStop { position: 1.0; color: "red" }
            }

    visible : true
}
