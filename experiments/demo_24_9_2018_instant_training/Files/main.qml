import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Controls 1.1

Window {
    visible: true
    width: 500
    height: 500
    id: root

    signal sendCanvasData(variant canvasdata);
    property variant imageDataQML;

    Row {
        id: tools

        Button {
            id: clear
            text: "Clear"
            onClicked: {
                canvas.clear()
            }
        }

        Button {
            id: save
            text: "Save"
            onClicked: {
                canvas.saveImage()
            }
        }

   }

    Canvas {

        id: canvas
        anchors.top: tools.bottom
        width: 500
        height: 500
        property int lastX: 0
        property int lastY: 0

//        property color fillStyle: "#ae32a0" // purple

        function clear(){
            var ctx = getContext("2d")
            ctx.reset()


            canvas.requestPaint()
            mouse.clear()
        }

        function saveImage (){
            var ctx = getContext("2d")
            var imageData = ctx.getImageData(10, 10, 90, 90)
            imageDataQML = ctx.getImageData(10, 10, 90, 90)
//            mouse.save (imageData)
            canvas.save("./test.png")
            mouse.myfun()
            mouse.getCanvasImageData(imageData.data)
           // console.log(imageData.data[2], imageData.height, imageData.width)

        }

        onPaint: {
            // do cool stuff here
            var ctx = getContext("2d")
            ctx.lineWidth = 3
//            ctx.strokeStyle = color.red
//            ctx.strokeStyle = "#ff000000" /*ctx.createPattern("#ff000000", Qt.Dense5Pattern)*/
            ctx.strokeStyle = "#ffffffff"

            ctx.strokeRect(10, 10, 100, 100)

            //  should be red, assignment failed, is green
//            ctx.fillStyle = "rgba(1.0,0.0,0.0,1.0)"
//            ctx.fillStyle = "rgba(0.99,0.0,0.0,0.1)"
            ctx.fillStyle = "rgba(0,0,0,0.01)"
            ctx.fillRect(10, 10, 100, 100)
            ctx.fill()

            ctx.beginPath()

            ctx.moveTo ( lastX, lastY )
            lastX = area.mouseX
            lastY = area.mouseY
            ctx.lineTo ( lastX, lastY )
            ctx.stroke ()
            mouse.pushpa()
            mouse.add( lastX. lastY)
            mouse.add (Qt.point( lastX, lastY ))
        }

        MouseArea{
            id: area
            anchors.fill: parent
            onPressed: {
                canvas.lastX = mouseX
                canvas.lastY = mouseY
            }

            onPositionChanged: {
                canvas.requestPaint()
            }
        }

    }
}
