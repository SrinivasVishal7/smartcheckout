from src.preprocessing import *
from src.predictcoin import CoinPrediction

import os
import sys
import time
# from src.CoinRecognitionUI import *

import sys

from PyQt4.QtCore import QDateTime, QObject, QUrl, pyqtSignal, QT_VERSION_STR, Qt
from PyQt4.QtGui import QApplication
from PyQt4.QtDeclarative import QDeclarativeView

from os import listdir
from os.path import isfile, join

print(QT_VERSION_STR)

app = QApplication(sys.argv)

# Create the QML user interface.
view = QDeclarativeView()
view.setSource(QUrl(SOURCE_DIR+'/qml/CoinRecognition.qml'))
view.setResizeMode(QDeclarativeView.SizeRootObjectToView)
view.showFullScreen()	
view.setWindowFlags(Qt.Window)

rootObject = view.rootObject()


def populateList():
    onlyfiles = [f for f in listdir(SOURCE_DIR+"/qml/images/anomalyframes/")]
    rootObject.populateList(onlyfiles)
    
def clearImages():
    """Code to clear recognition and anomalous image folders"""
    os.system("rm -rf qml/images/recognitionframes/* qml/images/anomalyframes/*")
    print("Images cleared...!!!")

def exitApplication():
    clearImages()
    os.system("pkill python")
    

rootObject.galleryClicked.connect(populateList)
rootObject.deleteImages.connect(clearImages)
rootObject.exitButtonClicked.connect(exitApplication)


# Display the user interface and allow the user to interact with it.
view.setGeometry(0, 0, 1920, 1080)
view.show()



# app = QApplication(sys.argv)
# coinrecognitionUI = CoinRecognitionUI()
# app.exec()

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

class FrameRecognizer(mp.Process):
    """Process to track and count the pills"""

    def run(self):

        global G_FRAMES_QUEUE
        global RECOGNITION_IMG_SIZE
        global COIN_PREDICTION
        global LOAD_CNN_MODEL
        global LOAD_ANOMALY_MODEL
        global LOAD_GROUP_ANOMALY_MODEL
        global CNN_MODEL_ID
        global ANOMALY_MODEL_ID
        global MODEL_TYPE


        COIN_PREDICTION = CoinPrediction(LOAD_CNN_MODEL, MODEL_TYPE, LOAD_ANOMALY_MODEL, LOAD_GROUP_ANOMALY_MODEL, CNN_MODEL_ID, ANOMALY_MODEL_ID)

        COIN_VALUES = [0, 5, 1, 10, 2]

        debug = False



        """Empty frame just to warm up the model"""
        recognition_frame = np.zeros((RECOGNITION_IMG_SIZE, RECOGNITION_IMG_SIZE, 3), dtype='uint8')
        features = COIN_PREDICTION.extract_features(recognition_frame)

        if LOAD_ANOMALY_MODEL:

            if LOAD_GROUP_ANOMALY_MODEL:
                COIN_PREDICTION.predict_anomaly(COIN_PREDICTION.group_ls_model, features)

            else:
                for anomaly_model in COIN_PREDICTION.individual_ls_models_list:
                    _ = COIN_PREDICTION.predict_anomaly(anomaly_model, features)

            if LOAD_CNN_MODEL:
                _, _ = COIN_PREDICTION.predict_coin(features)

        elif LOAD_CNN_MODEL:
            _, _ = COIN_PREDICTION.predict_coin(features)


        i = 0
        j = 0

        while True:
            if not G_FRAMES_QUEUE.empty():

                recognition_frame = G_FRAMES_QUEUE.get()
                predict_start = time.time()

                features = COIN_PREDICTION.extract_features(recognition_frame)

                is_coin = False

                """If ls anomaly is included"""
                if LOAD_ANOMALY_MODEL:

                    if LOAD_GROUP_ANOMALY_MODEL:

                        anomaly_prediction = COIN_PREDICTION.predict_anomaly(COIN_PREDICTION.group_ls_model, features)
                        is_coin = (anomaly_prediction == 0)

                        print("Group anomaly prediction ", is_coin)

                    else:

                        for anomaly_model, model_id in zip(COIN_PREDICTION.individual_ls_models_list, 		 COIN_PREDICTION.individual_ls_model_id_list):
                            anomoly_prediction = COIN_PREDICTION.predict_anomaly(anomaly_model, features)
                            print('model id : {}, anomaly_prediction : {}'.format(model_id, anomoly_prediction))

                            """If any one model says it is a coin, IT IS A COIN"""
                            if anomoly_prediction == 0:
                                #print('\n')
                                is_coin = True
                                break

                        print("Individual anomaly prediction ", is_coin)

                    if is_coin:
                        if LOAD_CNN_MODEL:

                            current_coin_class, confidence = COIN_PREDICTION.predict_coin(features)
                            print("Current coin class ", current_coin_class)

                    else:
                        print("Given contour is not a coin...!!!")
                        pass

                elif LOAD_CNN_MODEL:

                    """If no anomaly detection is included, object is a coin by default"""
                    is_coin = True

                    """Only CNN is included"""
                    current_coin_class, confidence = COIN_PREDICTION.predict_coin(features)

                if LOAD_CNN_MODEL:
                    
                    if is_coin:
                        current_coin_value = COIN_VALUES[ current_coin_class ]
                        if current_coin_value == 0:
                            is_coin = False
                            cv2.imwrite(SOURCE_DIR+"/qml/images/anomalyframes/anomalyframe_" + str(j) + ".png", recognition_frame)
                            j += 1
                        print("Overall time : ", time.time() - predict_start)
                    else:
                        current_coin_value = 0
                        cv2.imwrite(SOURCE_DIR+"/qml/images/anomalyframes/anomalyframe_" + str(j) + ".png", recognition_frame)
                        j += 1

                    cv2.imwrite(SOURCE_DIR+"/qml/images/recognitionframes/recognitionframe_" + str(i) + ".png", recognition_frame)
                    rootObject.update_ui_values(SOURCE_DIR+"/qml/images/recognitionframes/recognitionframe_" + str(i) + ".png", current_coin_value, is_coin)
                    i += 1

                #cv2.imshow("Recognition Frame", recognition_frame)

            key = cv2.waitKey(1)         
