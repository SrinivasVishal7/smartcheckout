import Qt 4.7

Rectangle {

    id : parentWindow
    visible: true
    width: 1920
    height: 1080

    property int sum : 0
    property int counterfeit_count : 0
    signal deleteImages()
    signal exitButtonClicked()
    signal galleryClicked()

    Rectangle {
        id : parentRect
        anchors.fill: parent
        color : "#EAEFF5"
        visible : true

        PropertyAnimation {
            id : parentRectPropertyAnimationStart
            target : parentRect
            property: "color"
            from : "#EAEFF5"
            to : "red"
            duration : 200
        }

        PropertyAnimation {
            id : parentRectPropertyAnimationEnd
            target : parentRect
            property: "color"
            from : "red"
            to : "#EAEFF5"
            duration : 200
        }

        Rectangle {
            id : titleRect
            width : parentWindow.width
            height: 100
            color: "#064E4B"

            Image {
                id : logoImage
                x : 10
                y : 10
                source : "images/logo.jpg"
            }

            Text {
                id : title
                anchors.centerIn: parent
                text : "Coin Recognition"
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 50
                color : "white"
            }
        }

        Rectangle {
            id : recognitionImageRect
            x : 50
            y : 200
            width : 400
            height : 400
            color : "white"
            radius: 5

            Image {
                id : recognitionImage
                anchors.centerIn: parent
                asynchronous: false
            }
        }

        Rectangle {
            id : recognitionImageRectTitle
            x : 50
            y : 150
            width : 400
            height : 50
            color : "#064E4B"
            radius: 5

            Text {
                id : titleText
                anchors.centerIn: parent
                text : "Recognition Image"
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 30
                color : "white"
            }
        }

        Rectangle {
            id : tenRupeeBorder
            x : 475
            y : 150
            width : 250
            height : 325
            radius: 5
            color : "white"
            border.color: "#064E4B"
            border.width: 5

            NumberAnimation {
                id : tenRupeeNumberAnimationExpand
                target : tenRupeeBorder
                from : 1
                to : 1.1
                property : "scale"
                duration : 200
            }

            NumberAnimation {
                id : tenRupeeNumberAnimationShrink
                target : tenRupeeBorder
                from : 1.1
                to : 1
                property : "scale"
                duration : 200
            }
        }

        Rectangle {
            id : fiveRupeeBorder
            x : 750
            y : 150
            width : 250
            height : 325
            radius: 5
            color : "white"
            border.color: "#064E4B"
            border.width: 5

            NumberAnimation {
                id : fiveRupeeNumberAnimationExpand
                target : fiveRupeeBorder
                from : 1
                to : 1.1
                property : "scale"
                duration : 200
            }

            NumberAnimation {
                id : fiveRupeeNumberAnimationShrink
                target : fiveRupeeBorder
                from : 1.1
                to : 1
                property : "scale"
                duration : 200
            }
        }

        Rectangle {
            id : twoRupeeBorder
            x : 1025
            y : 150
            width : 250
            height : 325
            radius: 5
            color : "white"
            border.color: "#064E4B"
            border.width: 5

            NumberAnimation {
                id : twoRupeeNumberAnimationExpand
                target : twoRupeeBorder
                from : 1
                to : 1.1
                property : "scale"
                duration : 200
            }

            NumberAnimation {
                id : twoRupeeNumberAnimationShrink
                target : twoRupeeBorder
                from : 1.1
                to : 1
                property : "scale"
                duration : 200
            }
        }

        Rectangle {
            id : oneRupeeBorder
            x : 1300
            y : 150
            width : 250
            height : 325
            radius: 5
            color : "white"
            border.color: "#064E4B"
            border.width: 5

            NumberAnimation {
                id : oneRupeeNumberAnimationExpand
                target : oneRupeeBorder
                from : 1
                to : 1.1
                property : "scale"
                duration : 200
            }

            NumberAnimation {
                id : oneRupeeNumberAnimationShrink
                target : oneRupeeBorder
                from : 1.1
                to : 1
                property : "scale"
                duration : 200
            }
        }

        Rectangle {
            id : totalRupeeBorder
            x : 1575
            y : 150
            width : 250
            height : 325
            radius: 5
            color : "white"
            border.color: "#064E4B"
            border.width: 5

            NumberAnimation {
                id : totalRupeeNumberAnimationExpand
                target : totalRupeeBorder
                from : 1
                to : 1.1
                property : "scale"
                duration : 200
            }

            NumberAnimation {
                id : totalRupeeNumberAnimationShrink
                target : totalRupeeBorder
                from : 1.1
                to : 1
                property : "scale"
                duration : 200
            }

            Text {
                id : totalCoinCountLabel
                text: "Total Coins "
                anchors.centerIn: parent
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 30
                color : "black"
            }
        }


        Image {
            id: tenRupeeImage
            source: "images/ten_rupee.jpeg"
            x : recognitionImageRect.x + recognitionImageRect.width + 100
            y : recognitionImageRect.y + 25
            scale : 2
        }

        Image {
            id: fiveRupeeImage
            source: "./images/five_rupee.jpg"
            x : tenRupeeImage.x + tenRupeeImage.width * 2 + 75
            y : recognitionImageRect.y + 25
            scale : 2
        }

        Image {
            id: twoRupeeImage
            source: "./images/two_rupee.jpeg"
            x : fiveRupeeImage.x + fiveRupeeImage.width * 2 + 75
            y : recognitionImageRect.y + 25
            scale : 2
        }


        Image {
            id: oneRupeeImage
            source: "./images/one_rupee.jpeg"
            x : twoRupeeImage.x + twoRupeeImage.width * 2 + 75
            y : recognitionImageRect.y + 25
            scale : 2
        }

        Counter {
            id : oneRupeeCounter1
            x : oneRupeeImage.x
            y : oneRupeeImage.y + oneRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"
        }

        Counter {
            id : oneRupeeCounter2
            x : oneRupeeCounter1.x + oneRupeeCounter1.width
            y : oneRupeeImage.y + oneRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                oneRupeeCounter1.incrementCount()
            }
        }

        Counter {
            id : oneRupeeCounter3
            x : oneRupeeCounter2.x + oneRupeeCounter2.width
            y : oneRupeeImage.y + oneRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                oneRupeeCounter2.incrementCount()
            }
        }

        Counter {
            id : tenRupeeCounter1
            x : tenRupeeImage.x
            y : tenRupeeImage.y + tenRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"
        }

        Counter {
            id : tenRupeeCounter2
            x : tenRupeeCounter1.x + tenRupeeCounter1.width
            y : tenRupeeImage.y + tenRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                tenRupeeCounter1.incrementCount()
            }
        }

        Counter {
            id : tenRupeeCounter3
            x : tenRupeeCounter2.x + tenRupeeCounter2.width
            y : tenRupeeImage.y + tenRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                tenRupeeCounter2.incrementCount()
            }
        }

        Counter {
            id : fiveRupeeCounter1
            x : fiveRupeeImage.x
            y : fiveRupeeImage.y + fiveRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"
        }

        Counter {
            id : fiveRupeeCounter2
            x : fiveRupeeCounter1.x + fiveRupeeCounter1.width
            y : fiveRupeeImage.y + fiveRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                fiveRupeeCounter1.incrementCount()
            }
        }

        Counter {
            id : fiveRupeeCounter3
            x : fiveRupeeCounter2.x + fiveRupeeCounter2.width
            y : fiveRupeeImage.y + fiveRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                fiveRupeeCounter2.incrementCount()
            }
        }

        Counter {
            id : twoRupeeCounter1
            x : twoRupeeImage.x
            y : twoRupeeImage.y + twoRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"
        }

        Counter {
            id : twoRupeeCounter2
            x : twoRupeeCounter1.x + twoRupeeCounter1.width
            y : twoRupeeImage.y + twoRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                twoRupeeCounter1.incrementCount()
            }
        }

        Counter {
            id : twoRupeeCounter3
            x : twoRupeeCounter2.x + twoRupeeCounter2.width
            y : twoRupeeImage.y + twoRupeeImage.height + 75
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                twoRupeeCounter2.incrementCount()
            }
        }

        Counter {
            id : totalCoinCounter1
            x : oneRupeeCounter3.x + oneRupeeCounter3.width + 175
            y : oneRupeeCounter3.y
            width : 30
            height : 50
            color : "red"
        }

        Counter {
            id : totalCoinCounter2
            x : totalCoinCounter1.x + totalCoinCounter1.width
            y : totalCoinCounter1.y
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                totalCoinCounter1.incrementCount()
            }
        }

        Counter {
            id : totalCoinCounter3
            x : totalCoinCounter2.x + totalCoinCounter2.width
            y : totalCoinCounter1.y
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                totalCoinCounter2.incrementCount()
            }
        }

        Counter {
            id : totalCoinCounter4
            x : totalCoinCounter3.x + totalCoinCounter3.width
            y : totalCoinCounter1.y
            width : 30
            height : 50
            color : "red"

            onDigitExceeded: {
                totalCoinCounter3.incrementCount()
            }
        }

        Rectangle {
            id : fakeCoinIndicatorRect
            x : recognitionImageRect.x
            y : recognitionImageRect.y + recognitionImageRect.height + 50
            width : 400
            height : 180
            radius : 5

            Image {
                id : fakeCoinIndicator
                anchors.fill: parent
                source : "images/fake.png"
                visible : false
            }
        }

        Rectangle {
            id : totalAmountLabelRect
            x : tenRupeeCounter1.x
            y : tenRupeeCounter1.y + tenRupeeCounter1.height + 75
            width : 400
            height : 100
            color : "#064E4B"
            radius: 5

            Text {
                id : totalAmountLabel
                text: "Total Amount "
                anchors.centerIn: parent
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 35
                color : "white"
            }
        }

        Rectangle {
            id : totalAmountRect
            x : totalAmountLabelRect.x + totalAmountLabelRect.width + 75
            y : totalAmountLabelRect.y
            width : 400
            height : 100
            color : "#064E4B"
            radius: 5

            Text {
                id : totalAmount
                property int value : sum
                anchors.centerIn: parent
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 40
                text : value
                color: "white"

                Behavior on value {
                    NumberAnimation { duration: 200; easing.type: Easing.InOutQuad }
                }
            }
        }

        Button {
            id : resetButton
            buttonText : "Reset"
            x : counterfeitCoinCountLabelRect.x
            y : counterfeitCoinCountLabelRect.y + counterfeitCoinCountLabelRect.height + 75
            width : 400
            height : 100
            radius : 5

            MouseArea {
                anchors.fill : parent

                onClicked: {
                    tenRupeeCounter1.resetCounter()
                    tenRupeeCounter2.resetCounter()
                    tenRupeeCounter3.resetCounter()

                    fiveRupeeCounter1.resetCounter()
                    fiveRupeeCounter2.resetCounter()
                    fiveRupeeCounter3.resetCounter()

                    twoRupeeCounter1.resetCounter()
                    twoRupeeCounter2.resetCounter()
                    twoRupeeCounter3.resetCounter()

                    oneRupeeCounter1.resetCounter()
                    oneRupeeCounter2.resetCounter()
                    oneRupeeCounter3.resetCounter()

                    totalCoinCounter1.resetCounter()
                    totalCoinCounter2.resetCounter()
                    totalCoinCounter3.resetCounter()
                    totalCoinCounter4.resetCounter()

                    sum = 0
                    counterfeit_count = 0

                    fakeCoinIndicator.visible = false
                    recognitionImage.source = ''

                    deleteImages()

                    imageListModel.clear()
                }
            }
        }

        Button {
            id : galleryButton
            buttonText : "Gallery"
            x : resetButton.x + resetButton.width + 75
            y : resetButton.y
            width : 400
            height : 100

            MouseArea {
                anchors.fill : parent

                onClicked: {
                    galleryClicked()
                }
            }
        }

        Button {
            id : exitButton
            buttonText : "Exit"
            x : galleryButton.x + galleryButton.width + 75
            y : galleryButton.y
            width : 400
            height : 100

            MouseArea {
                anchors.fill : parent

                onClicked: {
                    exitButtonClicked()
                }
            }
        }

        Rectangle {
            id : counterfeitCoinCountLabelRect
            x : totalAmountLabelRect.x
            y : totalAmountLabelRect.y + totalAmountLabelRect.height + 75
            width : 400
            height : 100
            color : "#064E4B"
            radius: 5

            Text {
                id : counterfeitCoinCountLabel
                text : "Anomalous Coin Count "
                anchors.centerIn: parent
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 35
                color: "white"
            }
        }

        Rectangle {
            id : counterfeitCoinCount

            x : counterfeitCoinCountLabelRect.x + counterfeitCoinCountLabelRect.width + 75
            y : counterfeitCoinCountLabelRect.y

            width : 400
            height : 100
            color : "#064E4B"
            radius: 5

            Text {
                id : counterfeitCoinCountValueLabel
                text : counterfeit_count
                anchors.centerIn: parent
                font.family: "Times New Roman"
                font.bold: true
                font.pixelSize: 40
                color: "white"
            }
        }

        ListModel {
            id : imageListModel
        }

        Rectangle {
            id : galleryDialog
            width : 1920
            height : 1080
            visible: false

            Rectangle {
                id : fillerRect1
                width : 1920
                height : 100
                anchors.top : parent.top
                anchors.left : parent.left
                color : "#064E4B"
                radius: 5
		visible : false

                Text {
                    id : sampleTitleText
                    text : "Anomalous Coins"
                    anchors.centerIn: parent
                    font.family: "Times New Roman"
                    font.bold: true
                    font.pixelSize: 50
                    color: "white"
		    visible : false
                }
            }

            Rectangle {
                id : fillerRect2
                width : 100
                height : galleryDialog.height
                anchors.top : parent.top
                anchors.left : parent.left
                color : "transparent"
            }

            Rectangle {
                id : fillerRect3
                width : 1920
                height : 50
                anchors.top : fillerRect1.bottom
                anchors.left : fillerRect1.left
                color : "transparent"
            }

            GridView {

                anchors.top : fillerRect3.bottom
                anchors.left : fillerRect2.right
                width : galleryDialog.width - 200
                height : galleryDialog.height - 200
                cellWidth : 400
                cellHeight : 400

                Component {
                    id: fileDelegate
                    Column {
                        Image {
                            width: 350; height: 350
                            fillMode: Image.PreserveAspectFit
                            smooth: true
                            source: path
                        }
                    }
                }

                model: imageListModel
                delegate: fileDelegate
            }

            onVisibleChanged: {
                if(!visible)
                {
                    parentWindow.visible = true
                }
            }

            MouseArea {
                anchors.fill : parent

                onClicked: {
                    galleryDialog.visible = false
                    parentWindow.visible = true
                }
            }
        }
    }

    function update_ui_values(recognition_frame, current_coin_value, is_coin)
    {
        recognitionImage.source = recognition_frame

        if( current_coin_value == 10 )
        {
            tenRupeeCounter3.incrementCount()
            totalCoinCounter4.incrementCount()
            tenRupeeNumberAnimationExpand.start()
            tenRupeeNumberAnimationShrink.start()

            totalRupeeNumberAnimationExpand.start()
            totalRupeeNumberAnimationShrink.start()
        }
        else if ( current_coin_value == 5 )
        {
            fiveRupeeCounter3.incrementCount()
            totalCoinCounter4.incrementCount()
            fiveRupeeNumberAnimationExpand.start()
            fiveRupeeNumberAnimationShrink.start()

            totalRupeeNumberAnimationExpand.start()
            totalRupeeNumberAnimationShrink.start()
        }
        else if ( current_coin_value == 2 )
        {
            twoRupeeCounter3.incrementCount()
            totalCoinCounter4.incrementCount()
            twoRupeeNumberAnimationExpand.start()
            twoRupeeNumberAnimationShrink.start()

            totalRupeeNumberAnimationExpand.start()
            totalRupeeNumberAnimationShrink.start()
        }
        else if ( current_coin_value == 1 )
        {
            oneRupeeCounter3.incrementCount()
            totalCoinCounter4.incrementCount()
            oneRupeeNumberAnimationExpand.start()
            oneRupeeNumberAnimationShrink.start()

            totalRupeeNumberAnimationExpand.start()
            totalRupeeNumberAnimationShrink.start()
        }
        else
        {
            console.log("Else case executed...!!!")
        }

        sum += current_coin_value

        if(is_coin == true)
        {
            fakeCoinIndicator.visible = false
        }
        else
        {
            parentRectPropertyAnimationStart.start()
            parentRectPropertyAnimationEnd.start()
            fakeCoinIndicator.visible = true

            counterfeit_count += 1
        }
    }

    function populateList( list )
    {
        console.log("I am list : ", list)

	imageListModel.clear()

        list.forEach(function (entry) {
            imageListModel.append( { path : "images/anomalyframes/" + entry } )
        });

        galleryDialog.visible = true
        parentWindow.visible = false
    }
}
