from OpenGL import GL
import sys
import cv2.cv2 as cv2
from PyQt5 import QtGui
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQuick import QQuickView

app = QApplication(sys.argv) 
view = QQuickView()
view.setSource(QUrl('preview.qml'))
rootObject = view.rootObject()
view.show()
app.exec_()
