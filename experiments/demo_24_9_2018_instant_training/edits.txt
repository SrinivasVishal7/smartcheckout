for value in points:
                x,y,w,h = value
                coords = x,y,x+w,y+h
                if tracking_started and len(predicted) > 0:
                     if current_objects > history_of_objects:
                        uoord = coordinate_to_update(coords, predicted)
                        if uoord is not None:
                            print("Initialising Tracker : ", history_of_objects)
                            rect = dlib.rectangle(uoord[0],uoord[1],uoord[2], uoord[3])
                            objects[history_of_objects]['tracker'].start_track(f, rect)
                            objects[history_of_objects]['tracker_id'] = history_of_objects
                            objects[history_of_objects]['tracking_state'] = True
                            history_of_objects += 1

                else:
                    print("Initialising Tracker : ", history_of_objects)
                    rect = dlib.rectangle(coords[0],coords[1],coords[2], coords[3])
                    objects[history_of_objects]['tracker'].start_track(f, rect)
                    objects[history_of_objects]['tracker_id'] = history_of_objects
                    objects[history_of_objects]['tracking_state'] = True
                    tracking_started = True
                    history_of_objects += 1