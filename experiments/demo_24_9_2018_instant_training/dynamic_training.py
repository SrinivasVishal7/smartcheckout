
# ----------------- Imports --------------------- #

from multiprocessing import Process
from multiprocessing import Queue
import cv2.cv2 as cv2
import numpy as np
import imutils
import os
from keras import Model
from keras.applications import VGG16
from keras.applications.imagenet_utils import preprocess_input
from sklearn.model_selection import train_test_split
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
import time

# ----------------- Globals -------------------- #

global MAX_QUEUE_SIZE, dataset_path, raw_data_path 
global SAMPLER_SIGNAL_QUEUE
global TRAIN_SIGNAL_QUEUE 
global FRAMES_QUEUE
global BG_QUEUE
global PREDICTION_QUEUE, LABEL_QUEUE

MAX_QUEUE_SIZE = 10
PREDICTION_QUEUE_SIZE = 1000

TRAIN_SIGNAL_QUEUE = Queue(MAX_QUEUE_SIZE)
SAMPLER_SIGNAL_QUEUE = Queue(MAX_QUEUE_SIZE)
FRAMES_QUEUE = Queue(MAX_QUEUE_SIZE)
BG_QUEUE = Queue(MAX_QUEUE_SIZE)
PREDICTION_QUEUE = Queue(PREDICTION_QUEUE_SIZE)
LABEL_QUEUE = Queue(PREDICTION_QUEUE_SIZE)

Sampler_Counter = 0
Is_Train_Pressed = False
Is_Prediction_Pressed = False

limit_points = [(220, 150), (520, 450)]
label = None
finalNumberOfImages = 100

dataset_path = "/home/ml/PycharmProjects/SmartCheckout/experiments/demo_24_9_2018_instant_training/dataset/augmented_dataset/"
raw_data_path = "/home/ml/PycharmProjects/SmartCheckout/experiments/demo_24_9_2018_instant_training/dataset/raw_dataset/"
npy_data_path = "/home/ml/PycharmProjects/SmartCheckout/experiments/demo_24_9_2018_instant_training/dataset/npys/"
model_path = "/home/ml/PycharmProjects/SmartCheckout/experiments/demo_24_9_2018_instant_training/dataset/models/best_model.h5"

# ------------------ Utility functions --------------------------#
def find_background(bg):
    avg_color_per_row = np.average(bg, axis=0)
    avg_color = np.average(avg_color_per_row, axis=0)
    return avg_color

def augment_data(bg):
    pixel = find_background(bg)
    pixel = pixel.astype(np.uint8)
    for subdirectory, directory ,files in os.walk(raw_data_path):
        if len(directory) != 0:
            for folder in directory:
                if not os.path.exists(dataset_path+folder):
                    os.mkdir(dataset_path+folder)
                    print("Class Directory '{}' Created".format(folder))
        elif len(directory) == 0:
            numberOfImages = len(files)
            angleDiff = int(int(360*numberOfImages)/finalNumberOfImages)
            imageCount = 0
            folder_name = subdirectory.split('/')[-1]
            for file in files:
                image = cv2.imread(os.path.join(subdirectory, file))
                for angle in np.arange(0, 360, angleDiff):
                    rotated = imutils.rotate(image, angle)
                    total_path = os.path.join(dataset_path, folder_name)
                    rotated[np.where((rotated==[0,0,0]).all(axis=2))] = pixel                    
                    cv2.imwrite(os.path.sep.join([total_path, str(imageCount) + "_" + str(angle) + ".bmp"]), rotated)
                    imageCount += 1
                    if imageCount == finalNumberOfImages:
                        break
                if imageCount == finalNumberOfImages:
                    break

def extract_features():
    
    model_vgg16 = VGG16(weights='imagenet', include_top=False)

    for subdirectory, directory ,files in os.walk(dataset_path):
        if len(directory) != 0:
            for folder in directory:
                if not os.path.exists(npy_data_path+folder):
                    os.mkdir(npy_data_path+folder)
                    print("Class Directory '{}' Created".format(folder))
        
        elif len(directory) == 0:
            
            final_features = []
            folder_name = subdirectory.split('/')[-1]
            print("Extracting features for {}".format(folder_name))
            
            for file in files:
                image = cv2.imread(os.path.join(subdirectory, file))
                img = np.expand_dims(image, axis=0)
                img = img.astype('float64')
                img = preprocess_input(img)
                features = model_vgg16.predict(img).flatten()
                final_features.append(features)

            final_features = np.array(final_features)
            total_path = os.path.join(npy_data_path, folder_name)
            np.save(os.path.sep.join([total_path, folder_name + ".npy"]), final_features)
    
    del model_vgg16
    label = 0
    final_features = np.empty((finalNumberOfImages,25088))
    final_label = np.empty((finalNumberOfImages,))

    for subdirs, dirs, files in os.walk(npy_data_path):
            if len(dirs) == 0:
                folder_name = subdirs.split('/')[-1]
                class_label = np.ones(100) * label
                print("Label for {} : {}".format(folder_name, label))
                label+=1
                final_label = np.hstack((final_label, class_label))
                for file in files:
                    total_path = os.path.join(npy_data_path, folder_name)
                    total_path = os.path.sep.join([total_path, file])
                    features = np.load(total_path)
                    final_features = np.vstack((final_features, features))  

    np.save(npy_data_path+'/'+'final_features',final_features)
    np.save(npy_data_path+'/'+'final_labels',final_label)
    

def load_data():
    feature = np.load(npy_data_path+'/'+'final_features.npy')
    labels = np.load(npy_data_path+'/'+'final_labels.npy')
    x_train, x_test, y_train, y_test = train_test_split(feature, labels, test_size=0.33, random_state=42)
    return (x_train, y_train), (x_test, y_test)

def neural_net():
    model = Sequential()
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(os.listdir(dataset_path)), activation='softmax'))
    return model

def train_features():
    (X_train, y_train), (X_test, y_test) = load_data()
    input_dim = 25088
    nb_epoch = 20
    model = neural_net()
    input_layer = Input(shape=(input_dim,))
    final_model = model(input_layer)
    full_model = Model(input=input_layer, output=final_model)
    
    adam = Adam(1e-4)

    checkpoint = ModelCheckpoint(model_path, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [checkpoint]

    full_model.compile(loss='sparse_categorical_crossentropy', optimizer=adam)
    full_model.fit(X_train, y_train,
            validation_data=(X_test, y_test),
            batch_size=32,                                                                                                                                                                                                                                                                                
            nb_epoch=nb_epoch,
            callbacks=callbacks_list, 
            verbose=1,
            shuffle=True)

def get_image_features(model,image):
    image = cv2.resize(image[limit_points[0][1]:limit_points[1][1],limit_points[0][0]:limit_points[1][0]], (224,224), cv2.INTER_AREA)
    img = np.expand_dims(image, axis=0)
    img = img.astype('float64')
    img = preprocess_input(img)
    features = model.predict(img).flatten()
    features = features.reshape(1, -1)
    return features



# ------------------ Process Functions ----------------------- #

def sampler(dirName,sampler_counter, frame):  
        raw_image = cv2.resize(frame[limit_points[0][1]:limit_points[1][1],limit_points[0][0]:limit_points[1][0]], (224,224), cv2.INTER_AREA)
        cv2.imwrite (dirName+'/'+"sample_"+str(sampler_counter)+".bmp", raw_image)

def TrainProcessor():
    bg_image = BG_QUEUE.get()
    TRAIN_SIGNAL_QUEUE.put(1)
    augment_data(bg_image)
    TRAIN_SIGNAL_QUEUE.put(2)
    extract_features()
    time.sleep(10)
    TRAIN_SIGNAL_QUEUE.put(3)
    train_features()
    TRAIN_SIGNAL_QUEUE.put(4)
    exit()


def PredictProcessor():
    model_vgg16 = VGG16(weights='imagenet', include_top=False)
    prediction_model = load_model(model_path, compile=False)
    labels = os.listdir(dataset_path)   
    while True:
        if not PREDICTION_QUEUE.empty():
            frame = PREDICTION_QUEUE.get()
            features = get_image_features(model_vgg16, frame)
            prediction = prediction_model.predict(features)
            predict_index = np.argmax(prediction, axis=1)[0]
            LABEL_QUEUE.put(labels[predict_index])

# ------------------- Main --------------------------- #

if __name__ == "__main__":
    trainProcess = Process(target=TrainProcessor)
    predictionProcess = Process(target=PredictProcessor)

    Sampler_Counter = 0
    Is_Train_Pressed = False
    Is_Prediction_Pressed = False
    
    text_on_top = ''
    font = cv2.FONT_HERSHEY_COMPLEX_SMALL
    label = ''
    cam = cv2.VideoCapture(0)

    bg_taken = False
    i = 0
    isPrediction = False
    class_label = ''

    frame_count = 0
    
    while True:

        if not TRAIN_SIGNAL_QUEUE.empty():
            val = TRAIN_SIGNAL_QUEUE.get()
            if val == 1:
                text_on_top = 'Augmenting Data..'
            elif val == 2:
                text_on_top = 'Extracting Features..'
            elif val == 3:
                text_on_top = 'Training Neural Net..'
            elif val == 4:
                text_on_top = 'Training Completed'
                if trainProcess.is_alive():
                    trainProcess.terminate()
                    time.sleep(1)
                    
        ret, frame = cam.read()
        frame_count += 1

        while not bg_taken:
            ret, bg_image = cam.read()
            i += 1
            if i == 30:
                bg_taken = True
                print("Back Ground Taken")
        
        cv2.putText(frame,'Operation : '+text_on_top,(140,40), font, 1,(255,0,0),1,cv2.LINE_AA)
        cv2.putText(frame,class_label,(300,200), font, 1,(0,255,0),1,cv2.LINE_AA)
        cv2.rectangle(frame, limit_points[0], limit_points[1], (0, 0, 0), 2)
        cv2.imshow ("Video Feed", frame)
        key = cv2.waitKey(1)

        if isPrediction == True and predictionProcess.is_alive():
            PREDICTION_QUEUE.put(frame)
            if not LABEL_QUEUE.empty():
                class_label = LABEL_QUEUE.get()
        else: 
            class_label = ''
        
        if chr(key & 255) is 'q':
            
            if trainProcess.is_alive():
                trainProcess.terminate()
                del trainProcess

            if predictionProcess.is_alive():
                predictionProcess.terminate()
                del predictionProcess
             
            break
        
        elif chr(key & 255) is 's':
            text_on_top = 'Sampling data'
            isPrediction = False
            Is_Train_Pressed = False
            Is_Prediction_Pressed = False

            if Sampler_Counter == 0:
                Sampler_Counter += 1    

                if predictionProcess.is_alive():
                    predictionProcess.terminate()
                    time.sleep(1)
                
                print ("----- Training Mode : Data Collection ------\n")
                
                try:
                    label = input ("Enter label : ")
                except EOFError:
                    print ("Error: EOF or empty input !")
                    label = ""
                
                dirName = raw_data_path + label
                
                if not os.path.exists(dirName):
                    os.mkdir(dirName)
                    print("Directory " , dirName ,  " Created")
                else:    
                    print("Directory " , dirName ,  " already exists")
            
            else : 
                sampler(dirName, Sampler_Counter, frame)
                

        elif chr(key & 255) is 't':    
            Sampler_Counter = 0
            isPrediction = False
            Is_Prediction_Pressed = False

            if not Is_Train_Pressed:
                
                if predictionProcess.is_alive():
                    predictionProcess.terminate()
                    time.sleep(1)
                
                Is_Train_Pressed = True
                BG_QUEUE.put(bg_image)
                trainProcess = Process(target=TrainProcessor)
                trainProcess.start()
                time.sleep(1)

        elif chr(key & 255) is 'p':
            text_on_top = 'Prediciting'
            Sampler_Counter = 0
            isPrediction = True
            Is_Train_Pressed = False

            if not Is_Prediction_Pressed:
                
                if trainProcess.is_alive():
                    trainProcess.terminate()
                    time.sleep(1)
                
                Is_Prediction_Pressed = True 
                predictionProcess = Process(target=PredictProcessor) 
                predictionProcess.start()
                time.sleep(1)

    raise SystemExit    
