import numpy as np
import cv2.cv2 as cv2
import dlib

video_capture = cv2.VideoCapture('/home/ml/Videos/Qtcam-18_10_01:15_44_19.avi')
counter = 0

while True:
    ret, frame = video_capture.read()

    if ret:
        counter += 1
        cv2.imwrite('./dataset/frames/image_'+str(counter)+'.bmp', frame)