import os 
from multiprocessing import Process
import keyboard
import time


def check_func():
    while True:
        c = 1+1

if __name__ == '__main__':
    check_process = Process(target=check_func)
    check_process.start() 
    print("Process status : {}".format(check_process.is_alive()))
    
    while True:
        
        print(check_process.is_alive())
        
        if keyboard.is_pressed('s'):
            check_process.terminate()
            check_process = Process(target=check_func)
            check_process.start()
            print("Process Restarting..")
            time.sleep(1)

        if keyboard.is_pressed('k'):
            check_process.terminate()
            print("Process Killed..")
            time.sleep(1)

        
        if keyboard.is_pressed('q'):
            check_process.terminate()
            break

    exit()




