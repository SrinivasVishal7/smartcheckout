import numpy as np
import cv2.cv2 as cv2
import dlib

mousePoints = []

def mouseEventHandler(event, x, y, flags, param):
    global mousePoints

    if event == cv2.EVENT_LBUTTONDOWN:
        mousePoints = [(x, y)]

    elif event == cv2.EVENT_LBUTTONUP:
        mousePoints.append((x, y))

video_capture = cv2.VideoCapture(0)

cv2.namedWindow("Webcam stream")
cv2.setMouseCallback("Webcam stream", mouseEventHandler)

tracker = dlib.correlation_tracker()

tracked = False

while True:
    ret, frame = video_capture.read()

    if ret:
        image = frame

        if len(mousePoints) == 2:
            cv2.rectangle(image, mousePoints[0], mousePoints[1], (0, 255, 0), 2)
            dlib_rect = dlib.rectangle(mousePoints[0][0], mousePoints[0][1], mousePoints[1][0], mousePoints[1][1])

        if tracked == True:
            tracker.update(image)
            track_rect = tracker.get_position()
            x  = int(track_rect.left())
            y  = int(track_rect.top())
            x1 = int(track_rect.right())
            y1 = int(track_rect.bottom())
            cv2.rectangle(image, (x, y), (x1, y1), (0, 0, 255), 2)

        cv2.imshow("Webcam stream", image)

    ch = 0xFF & cv2.waitKey(1)

    if ch == ord("r"):
        mousePoints = []
        tracked = False

    if ch == ord("t"):
        if len(mousePoints) == 2:
            tracker.start_track(image, dlib_rect)
            tracked = True
            mousePoints = []

    if ch == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()