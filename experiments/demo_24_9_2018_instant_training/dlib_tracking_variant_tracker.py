import cv2.cv2 as cv2
import numpy as np
import dlib
import math

# ---------------------- Globals -------------------------- # 

c = cv2.VideoCapture(0)
_, f = c.read() 

bg_taken = False

kernel = np.ones((3, 3), np.uint8)
objects = []

tracking_started = False
skip_limit = 100
frame_counter = 0
current_objects = 0
history_of_objects = 0
predicted_coords = []
current_coords = []
update_coords = []
object_counter = 0

# ------------------------ utils functions ----------------------- #

def detect_contours(img):
    points = []
    _, contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        area = cv2.contourArea(c)
        if area > 3000 and area < 50000 and is_valid_contour(c):
            x, y, w, h = cv2.boundingRect(c)
            pos = (x, y, x + w, y + h)
            points.append(pos)
    return points

def get_coordinates(img):
    diff = cv2.absdiff(img, bg_image)
    diff = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
    diff = cv2.medianBlur(diff, 5)
    _, fgmask = cv2.threshold(diff, 40, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel, iterations=2)
    points = detect_contours(fgmask)
    if len(points) > 0:
        return points, fgmask
    else:
        return None, fgmask

def is_valid_contour(contour, border=1):
    top_left_x, top_left_y, width, height = cv2.boundingRect(contour)
    
    if top_left_y - border >= 0 and top_left_y + height + border <= 480 and top_left_x - border >= 0 and top_left_x + width + border <= 640:
        return True
    else:
        return False

def check_distance(predicted, current):
    x, y, x1, y1 = predicted
    x2, y2, x3, y3 = current
    p1 = [(x + x1) / 2, (y + y1) / 2]
    p2 = [(x2 + x3) / 2, (y2 + y3) / 2]
    distance = math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2))
    return distance

def coordinate_to_update(c_coords, p_coords):
    if len(p_coords) > 0:
        for pcoord in p_coords:
            for ccoord in c_coords:
                if check_distance(pcoord, ccoord) < 50:
                    c_coords.remove(ccoord)
        return c_coords
    else:
        return c_coords

def fix_sanity(c_coords, trackers):    
    try:
        sane_flag = len(c_coords)
        tracker_count = len(trackers)
    except:
        sane_flag = 0
        tracker_count = 0

    if tracker_count > 0 and sane_flag > 0:
        for tracker in trackers:
            is_match_found = False
            for value in c_coords:
                rect = tracker['tracker'].get_position()
                x = int(rect.left())
                y = int(rect.top())
                x1 = int(rect.right())
                y1 = int(rect.bottom())
                poords = (x, y, x1, y1)
                distance = check_distance(poords, value)
                if distance < 50:
                    is_match_found = True
            if not is_match_found:
                trackers.remove(tracker)

        return trackers

def check_sanity(c_coords, p_coords):
    flag_set = []
    if len(p_coords) > 0 and len(c_coords) > 0:
        for ccoord in c_coords:
            flags = []
            for pcoord in p_coords:
                if check_distance(pcoord, ccoord) >= 50:
                    flags.append(False)
                if check_distance(pcoord, ccoord) < 50:
                    flags.append(True)
            flag_set.append(flags)

        for flag in flag_set:
            flag.sort()

        if len(flag_set[0]) == 1:
            return flag_set[0][0]

        return all(x == flag_set[0] for x in flag_set)
    else:
        return True

# ------------------------- Main ------------------------------ # 

while (1):
    _, f = c.read()

    while not bg_taken:
        ret, bg_image = c.read()
        frame_counter += 1
        if frame_counter == 30:
            bg_taken = True
            frame_counter = 0
            print("Back Ground Taken")

    frame_counter += 1

    # ------------ Object detection Detection ----------- #

    current_coords, fgmask = get_coordinates(f)
    try:
        current_objects = len(current_coords)
        history_of_objects = len(objects)
    except:
        current_objects = 0
        history_of_objects = 0

    print("Current: ", current_objects, " History: ", history_of_objects)

    # ------------ Tracker Prediction & Sanity Check ------------ #

    if tracking_started and history_of_objects > 0:

        is_sane = True
        
        if frame_counter % 5 == 0:
            is_sane = check_sanity(current_coords, predicted_coords)

        if not is_sane or history_of_objects > current_objects:
            objects = fix_sanity(current_coords, objects)

        predicted_coords = []

        for i, tracker in enumerate(objects):
            tracker['tracker'].update(f)
            track_rect = tracker['tracker'].get_position()
            x = int(track_rect.left())
            y = int(track_rect.top())
            x1 = int(track_rect.right())
            y1 = int(track_rect.bottom())
            poords = (x, y, x1, y1)
            predicted_coords.append(poords)

    # ----------- Tracker initialisation ----------- #

    if current_objects > history_of_objects:
        if frame_counter > skip_limit:
            tracking_started = True
            update_coords = coordinate_to_update(current_coords, predicted_coords)
            if len(update_coords) > 0:
                for uoord in update_coords:
                    cv2.imshow('new object', f[uoord[1]:uoord[3], uoord[0]:uoord[2]])
                    cv2.imwrite('new_object_'+str(object_counter)+'.bmp',f[uoord[1]:uoord[3], uoord[0]:uoord[2]])
                    object_counter+=1
                    bbox = dlib.rectangle(uoord[0], uoord[1], uoord[2], uoord[3])
                    object_prop = {
                        'tracker': dlib.correlation_tracker(),
                        'tracking_state': False
                    }
                    object_prop['tracker'].start_track(f, bbox)
                    object_prop['tracking_state'] = True
                    objects.append(object_prop)
                    del object_prop

    # ------------ Display ------------ #

    # if current_objects > 0:
    #     for value in update_coords:
    #             x,y,x1,y1 = value
    #             cv2.rectangle(f, (x, y), (x1, y1), (0, 255, 0), 2)

    if history_of_objects > 0:
        for value in predicted_coords:
            x, y, x1, y1 = value
            cv2.rectangle(f, (x, y), (x1, y1), (0, 0, 255), 2)

    cv2.imshow("frame", f)
    cv2.imshow("mask", fgmask)

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cv2.destroyAllWindows()
c.release()
