import numpy as np
import cv2.cv2 as cv2

cap = cv2.VideoCapture(0)

def augment_data():
    for subdirectory, directory ,files in os.walk(raw_data_path):
        if len(directory) != 0:
            for folder in directory:
                if not os.path.exists(dataset_path+folder):
                    os.mkdir(dataset_path+folder)
                    print("Class Directory '{}' Created".format(folder))
        elif len(directory) == 0:
            numberOfImages = len(files)
            angleDiff = int(int(360*numberOfImages)/finalNumberOfImages)
            imageCount = 0
            folder_name = subdirectory.split('/')[-1]
            for file in files:
                image = cv2.imread(os.path.join(subdirectory, file))
                for angle in np.arange(0, 360, angleDiff):
                    rotated = imutils.rotate(image, angle)
                    total_path = os.path.join(dataset_path, folder_name)
                    cv2.imwrite(os.path.sep.join([total_path, str(imageCount) + "_" + str(angle) + ".bmp"]), rotated)
                    imageCount += 1
                    if imageCount == finalNumberOfImages:
                        break
                if imageCount == finalNumberOfImages:
                    break


while True:
    ret, frame = cap.read()
    cv2.imshow('frame',frame)
    
    k = cv2.waitKey(1) & 0xFF
    
    if k == ord('q'):
        break

    if k == ord('s'):
        

cap.release()
cv2.destroyAllWindows()