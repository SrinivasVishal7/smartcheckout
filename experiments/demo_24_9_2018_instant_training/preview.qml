import QtQuick 2.11

Rectangle {
    id : bigRect
    width : 800
    height : 600
    color : 'light blue'
    focus : true

    Canvas {
        id: mycanvas
        width: 700
        height: 500

        onPaint: {
            var ctx = getContext("2d");
            ctx.fillStyle = Qt.rgba(0, 0, 0, 1);
            ctx.fillRect(100, 100, width, height);
        }
    }

    function update_canvas(frame){
        console.log(frame);
        var ctx = mycanvas.getContext("2d");
        console.log(ctx)
        // mycanvas.repaint();

    }
}